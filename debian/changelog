ctdconverter (2.1-8) unstable; urgency=medium

  * Team upload.
  * d/{control,p/add_setup.py.patch}: remove pkg_resources. (Closes: #1083347)
  * d/py3dist-overrides: remove: now unneeded.
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Wed, 09 Oct 2024 17:15:10 +0200

ctdconverter (2.1-7) unstable; urgency=medium

  * Team upload.
  * Do a one shot final 2to3 conversion (Closes: #1045042)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 19 Jun 2024 11:54:31 +0200

ctdconverter (2.1-6) unstable; urgency=medium

  * Team upload.
  * Fix Python3 syntax
    Closes: #1040294
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 13 Jul 2023 17:35:37 +0200

ctdconverter (2.1-5) unstable; urgency=medium

  * Team upload.
  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 02 Nov 2022 14:38:29 +0100

ctdconverter (2.1-4) unstable; urgency=medium

  * Team upload.
  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 14 Oct 2021 09:14:22 +0200

ctdconverter (2.1-3) unstable; urgency=medium

  * Team upload.
  * Restrictions: superficial
    Closes: #971459
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 01 Oct 2020 11:29:40 +0200

ctdconverter (2.1-2) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 19 Apr 2020 19:01:29 +0200

ctdconverter (2.1-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Remove unnecessary X-Python{,3}-Version field in debian/control.

  [ Michael R. Crusoe ]
  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 23 Jul 2019 11:20:16 +0200

ctdconverter (2.0-4) unstable; urgency=medium

  * Add missing dependency on pkg_resources, fixes AutoPkgTest

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 25 Feb 2018 09:49:18 -0800

ctdconverter (2.0-3) unstable; urgency=medium

  * Add missing build-dep on 2to3
  * Quiet 2to3 conversion

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 22 Feb 2018 08:19:00 -0800

ctdconverter (2.0-2) unstable; urgency=medium

  * Update VCS-*
  * fix AutoPkgTests
  * Add missing dependency on ctdopts

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 21 Feb 2018 01:25:52 -0800

ctdconverter (2.0-1) unstable; urgency=medium

  * Initial release. (Closes: #890755)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 18 Feb 2018 02:17:55 -0800
